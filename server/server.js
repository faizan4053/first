
//importing configuration file
const config=require('./config/config');
const route=require('./routes/route');
const express=require('express');
const bodyParser=require('body-parser');
const cors=require('cors');
const cookieParser=require('cookie-parser');
const hash=require('password-hash');
const sessions=require('express-session');

const app=express();

app.use(cookieParser());

app.use(sessions({
    secret:'innofarms',
    saveUninitialized:true,
    resave:false,
}))

app.use(cors());
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));

//redirecting to 
app.use('/user',route);



//set up the server to listen at port 4000
app.listen(config.port,()=>{
    console.log('server is listening at port '+config.port);
});








