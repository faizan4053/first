import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PostProcessingPage } from './post-processing.page';

describe('PostProcessingPage', () => {
  let component: PostProcessingPage;
  let fixture: ComponentFixture<PostProcessingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PostProcessingPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PostProcessingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
