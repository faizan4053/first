import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PostProcessingPage } from './post-processing.page';

const routes: Routes = [
  {
    path: '',
    component: PostProcessingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PostProcessingPageRoutingModule {}
