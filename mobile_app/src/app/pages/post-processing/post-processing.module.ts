import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PostProcessingPageRoutingModule } from './post-processing-routing.module';

import { PostProcessingPage } from './post-processing.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PostProcessingPageRoutingModule
  ],
  declarations: [PostProcessingPage]
})
export class PostProcessingPageModule {}
