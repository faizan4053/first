import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient, HttpHeaders } from '@angular/common/http';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {
  name:string;
  password:string;
  constructor(private route: Router,public httpClient: HttpClient) { }

  //function to handle login request
  dashboard() {

    var headers = new HttpHeaders();
    headers.append("Accept", 'application/json');
    headers.append('Content-Type', 'application/json' );
    //const requestOptions = new RequestOptions({ headers: headers });

    let postData = {
            name: this.name,
            password:this.password,
    }

    this.httpClient.post("http://127.0.0.1:4000/user/login", postData,{headers: headers })
      .subscribe(data => {
        console.log(data);
        //alert(data);
        if(data!='not ok'){
          console.log('its working');
          this.route.navigate(['/home']);
        }
       }, error => {
        console.log(error);
      });

    // this.route.navigate(['/home']);
  }

  ngOnInit() {
  }

}
