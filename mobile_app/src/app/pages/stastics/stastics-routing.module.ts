import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { StasticsPage } from './stastics.page';

const routes: Routes = [
  {
    path: '',
    component: StasticsPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class StasticsPageRoutingModule {}
