import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { StasticsPageRoutingModule } from './stastics-routing.module';

import { StasticsPage } from './stastics.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    StasticsPageRoutingModule
  ],
  declarations: [StasticsPage]
})
export class StasticsPageModule {}
