import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { StasticsPage } from './stastics.page';

describe('StasticsPage', () => {
  let component: StasticsPage;
  let fixture: ComponentFixture<StasticsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StasticsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(StasticsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
