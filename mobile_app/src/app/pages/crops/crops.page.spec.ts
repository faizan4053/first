import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { CropsPage } from './crops.page';

describe('CropsPage', () => {
  let component: CropsPage;
  let fixture: ComponentFixture<CropsPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CropsPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(CropsPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
