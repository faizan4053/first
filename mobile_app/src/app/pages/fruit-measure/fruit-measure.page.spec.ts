import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FruitMeasurePage } from './fruit-measure.page';

describe('FruitMeasurePage', () => {
  let component: FruitMeasurePage;
  let fixture: ComponentFixture<FruitMeasurePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FruitMeasurePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FruitMeasurePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
