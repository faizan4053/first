import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FruitMeasurePage } from './fruit-measure.page';

const routes: Routes = [
  {
    path: '',
    component: FruitMeasurePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FruitMeasurePageRoutingModule {}
