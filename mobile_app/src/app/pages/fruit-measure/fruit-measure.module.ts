import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FruitMeasurePageRoutingModule } from './fruit-measure-routing.module';

import { FruitMeasurePage } from './fruit-measure.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FruitMeasurePageRoutingModule
  ],
  declarations: [FruitMeasurePage]
})
export class FruitMeasurePageModule {}
