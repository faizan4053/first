import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { PreProcessingPage } from './pre-processing.page';

const routes: Routes = [
  {
    path: '',
    component: PreProcessingPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PreProcessingPageRoutingModule {}
