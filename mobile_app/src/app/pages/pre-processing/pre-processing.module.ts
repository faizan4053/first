import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { PreProcessingPageRoutingModule } from './pre-processing-routing.module';

import { PreProcessingPage } from './pre-processing.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    PreProcessingPageRoutingModule
  ],
  declarations: [PreProcessingPage]
})
export class PreProcessingPageModule {}
