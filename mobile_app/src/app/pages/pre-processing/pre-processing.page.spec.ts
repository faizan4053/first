import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { PreProcessingPage } from './pre-processing.page';

describe('PreProcessingPage', () => {
  let component: PreProcessingPage;
  let fixture: ComponentFixture<PreProcessingPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreProcessingPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(PreProcessingPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
