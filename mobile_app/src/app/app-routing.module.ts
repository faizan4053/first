import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: 'login',
    loadChildren: () => import('./pages/login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: '',
    redirectTo: 'login',
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)
  },
  {
    path: 'crops',
    loadChildren: () => import('./pages/crops/crops.module').then( m => m.CropsPageModule)
  },
  {
    path: 'fruit-measure',
    loadChildren: () => import('./pages/fruit-measure/fruit-measure.module').then( m => m.FruitMeasurePageModule)
  },
  {
    path: 'pre-processing',
    loadChildren: () => import('./pages/pre-processing/pre-processing.module').then( m => m.PreProcessingPageModule)
  },
  {
    path: 'processing',
    loadChildren: () => import('./pages/processing/processing.module').then( m => m.ProcessingPageModule)
  },
  {
    path: 'packaging',
    loadChildren: () => import('./pages/packaging/packaging.module').then( m => m.PackagingPageModule)
  },
  {
    path: 'post-processing',
    loadChildren: () => import('./pages/post-processing/post-processing.module').then( m => m.PostProcessingPageModule)
  },
  {
    path: 'stastics',
    loadChildren: () => import('./pages/stastics/stastics.module').then( m => m.StasticsPageModule)
  },
  {
    path: 'settings',
    loadChildren: () => import('./pages/settings/settings.module').then( m => m.SettingsPageModule)
  },
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule { }
